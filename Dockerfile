FROM alpine:3.8

RUN apk add --update \
    openssl \
    curl \
    php7 \
    php7-phar \
    php7-openssl \
    php7-iconv \
    php7-mbstring \
    php7-json \
    php7-tokenizer \
    php7-simplexml \
    php7-dom \
    php7-xmlwriter \
    php7-xml

# Install composer (as we often need to install dependencies to run tests).
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('SHA384', 'composer-setup.php') === '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN php -r "unlink('composer-setup.php');"

# Make the working directory.
RUN mkdir /work
WORKDIR /work
